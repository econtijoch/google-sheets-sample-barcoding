# Google Sheets Sample Barcoding

The barcode_script.js file contains the code necesary for creating a Google Sheets add-on for generating and exporting barcode labels. These labels can help in tracking and streamlining sample collection and data generation.

The exporting functionality of this code has been developed for use with Google Sheets and LabelMark5 software for Brady label printers. For broader use, it may be most convenient to simply use the BarcodeID() function to generate barcodes that can be exported and printed with the available resources.