// A quick function that will trigger the authorization prompt that is
// needed to gain access to the Export capabilities of the tool
function getStarted() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var csvMenuEntries = [{name: "Export file as Labels.csv", functionName: "saveAsCSV"}];
  var barcodeMenuEntries = [{name: "How to add barcodes", functionName: "howToAddBarcodes"},
                            {name: "How to export", functionName: "howToExport"}];
  ss.addMenu("Export", csvMenuEntries);
  ss.addMenu("Barcode Help", barcodeMenuEntries);
}

/**
 * Adds a custom menu to the active spreadsheet, containing a single menu item
 * for invoking the readRows() function specified above.
 * The onOpen() function, when defined, is automatically invoked whenever the
 * spreadsheet is opened.
 * For more information on using the Spreadsheet API, see
 * https://developers.google.com/apps-script/service_spreadsheet
 */
function onOpen() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var csvMenuEntries = [{name: "Export file as Labels.csv", functionName: "saveAsCSV"}];
  var barcodeMenuEntries = [{name: "How to add barcodes", functionName: "howToAddBarcodes"},
                            {name: "How to export", functionName: "howToExport"}, 
                            {name: "How to access old labels", functionName: "whereLabels"}];
  ss.addMenu("Export", csvMenuEntries);
  ss.addMenu("Barcode Help", barcodeMenuEntries);
  ss.setActiveSheet(ss.getSheets()[0]);
  
}

// Test function to debug the code above
function test() {}

/**
 * Retrieves all the rows in the active spreadsheet that contain data and logs the
 * values for each row.
 * For more information on using the Spreadsheet API, see
 * https://developers.google.com/apps-script/service_spreadsheet
 */
function readRows() {
  var sheet = SpreadsheetApp.getActiveSheet();
  var rows = sheet.getDataRange();
  var numRows = rows.getNumRows();
  var values = rows.getValues();

  for (var i = 0; i <= numRows - 1; i++) {
    var row = values[i];
    Logger.log(row);
  }
};

// Message box for menu option "How to add barcodes"
function howToAddBarcodes() {
  Browser.msgBox("How to make more barcodes", "To make more barcodes, simply include the \
information in the first three columns and use the spreadsheet function \
\"=BarcodeID(Experiment, Sample, Date)\" to generate the barcode ID.", Browser.Buttons.OK);
}

// Message box for menu option "How to export"
function howToExport() {
  Browser.msgBox("How to export your file", "First, be sure there are no errors in the \
spreadsheet. Then, to export your file, select the \"Export\" menu item and select \
\"Export file as Labels.csv\".This will create a new file in your google drive that will \
be named \"Labels.csv\".\n\n Download this and use it to import it to the LabelMark \
Software.", Browser.Buttons.OK); 
}

// Message box for menu option "How to access old labels"
function whereLabels() {
  Browser.msgBox("How to access old labels", "To access labels made previously, select the sheet labeled \
\"Master Sheet\" below. This will store the information that was exported to \"Labels.csv\" and will not \
generate new random strings with every page refresh.", Browser.Buttons.OK);
}

/**
* Returns a random upper or lowercase letter or number
*/
function RandomChar() {
  chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  var letter = chars.charAt(Math.random()*(0,chars.length - 1))
  return letter
}

/**
* Returns a string comprised of random characters of a length determined by the input
*/
function RandomString(length) {
  var rands = "";
  var z = length -1;
  for (var j = 0; j <= z; j++) {
    rands += RandomChar();
  }
  return rands;
}

// Returns a two-digit value for the month from a date
function TwoMonth(date) {
  var month = date.getUTCMonth() + 1;
  if (month < 10) {
    month = "0" + month;
  }
  return month;
}

// Returns a two-digit value for the day of the month from a date
function TwoDay(date) {
  var day = date.getUTCDate();
  if (day < 10) {
    day = "0" + day;
  }
  return day;
}

// Returns a two-digit value for the year from a date
function TwoYear(date) {
  var year = (date.getFullYear() +"").substring(2,4);
  return year;
}

// Creates a string of length exactly equal to 5. Truncates after the third character if
// it is too long, adds "amend" in front of string if length is less than 5 if location = f, or
// at the end of the string if location = b.
function xString(string, amend, location, x) { 
  string = string.toString();
  amend = amend.toString();
  var len = string.length;
  var newstring = new String();
  if (len > x) {
    string = string.substring(0,x);
  }
  if (len == x) {}
  if (len < x) {
    for (var j = 0; j < x - len; j++) {
      if (location == "f") {
      string = amend + string;
      }
      else if (location == "b") {
      string = string + amend;
      }
    }
  }
  newstring = string;
  return newstring;
}


// Creates a 20 character barcode string including the Experiment ID, Sample ID, Date,
// and a random 5 character string.
function BarcodeID(experiment, sample, date) {
  
  var barcode = new String();
  var rand = RandomString(5);     // Creates a random string of length 5
  experiment = String(experiment); // Coerces experiment variable into string, just in case.
  
  if (experiment == "") {
    experiment = "EXPID";
  }
  if (sample == "") {
    sample = "00000";
  }
  
  var expLength = experiment.length;
  
  var month = "MM"; // sets the default for the month to MM
  var day = "DD";   // sets the default for the day to DD
  var year = "YY";  // sets the default for the year to YY
 
  if (date instanceof Date) {
    month = TwoMonth(date);    // Gets month in two digit format
    day = TwoDay(date);        // Gets day of the month in two digit format
    year = TwoYear(date);      // Gets year in two-digit format
  }
  
 
  if (expLength >= 16) {
    barcode = experiment.substring(0,11) + "|" + month + day + year + "|" + rand;
  } else if (expLength < 16) {
    var counter = 16;
    counter -= (expLength + 1);
    sample = xString(sample,"0", "f", counter);
    
    barcode = experiment + "|" + sample + "|" + month + day + year + "|" + rand;
  }
    
  Utilities.sleep(1000); // prevents google server from giving "too many times" error
  return barcode;  
}

  
// The following is a script modified from the Google Scripts tutorial that will 
// save a spreadsheet to a CSV with rows titled "Labels.csv" and add the labels to the Master List
function saveAsCSV() {
  // Sets the filename to "Labels.csv"
  var fileName = "Labels.csv";
  // Convert the range data to CSV format
  var csvFile = convertToCsvFile_(fileName);
  // Create a file in the Docs List with the given name and the CSV data
  DriveApp.createFile(fileName, csvFile);
  
  // The following copies the values to the master list 
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheetByName("Label Maker")
  var values = sheet.getSheetValues(2, 1, -1, -1);
  ss.setActiveSheet(ss.getSheetByName("Master List (New 11/2014 - Present)"));
  
  for (var row = 0; row < values.length; row++) {
    ss.appendRow(values[row]);
  }
  ss.setActiveSheet(ss.getSheetByName("Label Maker"));
}

// Converts the spreadsheet to a CSV formatted with commas separating 
// columns and carriage returns separating rows
function convertToCsvFile_(csvFileName) {
  // Get the selected range in the spreadsheet
  var ws = SpreadsheetApp.getActiveSheet();
  try {
    var data = ws.getDataRange().getValues();
    var csvFile = undefined;

    // Loop through the data in the range and build a string with the CSV data
    if (data.length > 1) {
      var csv = "";
      for (var row = 2; row < data.length; row++) {
        for (var col = 0; col < data[row].length; col++) {
          if (data[row][col] == "Experiment ID") {  // Unless it is the Experiment ID Header cell
          row +=1;
          }
          else if (data[row][col] == "Sample #") {  // Unless it is the Sample # Header cell
          row +=1;
          }
          else if (data[row][col] == "Date") {      // Unless it is the Date Header cell
          row +=1;
          }
          else if (data[row][col] == "Barcode") {   // Unless it is the Barcode Header cell
          row +=1;
          }
          else if (data[row][col] == undefined){    // Unless it is empty
          row +=1;
          }
          else {
            if (data[row][col] instanceof Date) {   // Changes a date into MM/DD/YY format
              data[row][col] = TwoMonth(data[row][col]) + "|" + TwoDay(data[row][col]) + "|" + data[row][col].getFullYear();
            }
            if (data[row][col].toString().indexOf(",") != -1 && data[row][col] != undefined) {  // Builds String
                data[row][col] = "\"" + data[row][col] + "\"";
              }
            }
        }

        // Join each row's columns
        // Add a carriage return to end of each row, except for the last one
        if (row < data.length-1) {
          csv += data[row][1] + "," +  data[row].join(",") + "\r\n";
        }
        else {
          csv += data[row][1] + "," +  data[row];
        }
      }
      csvFile = csv;
    }
    return csvFile;
  }
  catch(err) {
    Logger.log(err);
    Browser.msgBox(err);
  }
}

function makePlate(samples, size) {
  if (size == 384) {
    var rows = 16;
    var columns = 24;
  }
  else {
    var rows = 8;
    var columns = 12;
  }
  var plate = new Array(rows);
  for (var row = 0; row < plate.length; row++) {
    plate[row] = new Array(columns);
    var index = 12 * (row);
    for (var col = 0; col < plate[row].length; col++) {
      plate[row][col] = samples[index + (col)];
    }
  }
  return plate;
}